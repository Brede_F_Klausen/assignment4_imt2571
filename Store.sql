-- Made by Brede Fritjof Klausen
-- FIlename: Store.sql
-- StudNr:   473211
-- Course:   IMT2571
-- Date:     22.10.2017
DROP SCHEMA

IF EXISTS task2;
  CREATE SCHEMA task2 COLLATE = utf8_general_ci;

USE task2;

CREATE TABLE store (
  `storeId`   int          NOT NULL AUTO_INCREMENT,
  `name`      varchar(60)  NOT NULL UNIQUE,
  `email`     varchar(250) NOT NULL,
  `address`   varchar(250) NOT NULL,
  PRIMARY KEY(storeId)
);

CREATE TABLE manufacturer (
  `maName`      varchar(16)  NOT NULL,
  `country`     varchar(2)   NOT NULL,
  `serviceURL`  varchar(250) NOT NULL,
  PRIMARY KEY(maName)
);

CREATE TABLE assesser (
  `asrId`       int          NOT NULL AUTO_INCREMENT,
  `asrName`   varchar(60)    NOT NULL REFERENCES store,
  `email`       varchar(250) NOT NULL,
  `mobileNo`    varchar(32)  NOT NULL,
  `address`     varchar(250) NOT NULL,
  PRIMARY KEY(asrID)
);

CREATE TABLE asseses (
  `maName`     varchar(16) NOT NULL REFERENCES manufacturer,
  `asId`      int          NOT NULL REFERENCES assesser,
  `storeId`    int         NOT NULL REFERENCES store,
  `asDate`    date         NOT NULL,
  PRIMARY KEY(maName, asId, storeId)
);

CREATE TABLE customer (
  `email`     varchar(250) NOT NULL,
  `name`      varchar(60)  NOT NULL,
  `mobileNo`  varchar(32)  NOT NULL,
  `address`   varchar(250) NOT NULL,
  PRIMARY KEY(email)
);

CREATE TABLE telephone (
  `storeId`   int         NOT NULL REFERENCES store,
  `pNumber`   varchar(32) NOT NULL,
  PRIMARY KEY(storeId, pNumber)
);

CREATE TABLE lease (
  `storeId`     int   NOT NULL REFERENCES store,
  `leaseNo`     int UNSIGNED NOT NULL,
  `startDate`   date  NOT NULL,
  `endDate`     date  NOT NULL,
  `totalPrice`  int,
  `status`      enum('active', 'finished') NOT NULL,
  `email`       varchar(250) NOT NULL REFERENCES customer,
  PRIMARY KEY(storeId, leaseNo)
);

CREATE TABLE lensMount (
  `id`          int         NOT NULL,
  `name`        varchar(16) NOT NULL UNIQUE,
  `maName`      varchar(16) NOT NULL REFERENCES manufacturer,
  PRIMARY KEY(id)
);

CREATE TABLE lensType (
  `lensId`        int         NOT NULL AUTO_INCREMENT,
  `focalLength`   varchar(16) NOT NULL,
  `maxAperture`   dec(4,1)    NOT NULL,
  `length`        int         NOT NULL,
  `filterSize`    int         NOT NULL,
  PRIMARY KEY(lensId)
);

CREATE TABLE equipmentType (
  `maName`        varchar(16)  NOT NULL REFERENCES manufacturer,
  `typeName`      varchar(60)  NOT NULL,
  `weigth`        int UNSIGNED NOT NULL,
  `perDayPrice`   dec(7,2)     NOT NULL,
  `megaPixel`     dec(4,1)     DEFAULT 1,
  `lensID`        int          DEFAULT NULL REFERENCES lensType,
  `mountId`       int          NOT NULL REFERENCES lensMount,
  PRIMARY KEY(maName, typeName)
);

CREATE TABLE equipment (
  `serialNo`    int UNSIGNED NOT NULL,
  `startDate`   date         NOT NULL ,
  `status`      enum('working', 'defect') NOT NULL,
  `state`       int check(state >= 1 AND state <=5),
  `maName`      varchar(16)  NOT NULL REFERENCES manufacturer,
  `typeName`    varchar(60)  NOT NULL REFERENCES equipmentType,
  `storeId`     int          NOT NULL REFERENCES store,
  `leaseNo`     int          REFERENCES lease,
  PRIMARY KEY(serialNo)
);

CREATE TABLE price (
  `storeId`     int NOT NULL REFERENCES store,
  `leaseNo`     int NOT NULL REFERENCES lease,
  `extraFee`    int,
  `explenation` varchar(2000),
  PRIMARY KEY(storeId, leaseNo)
);

CREATE TABLE memoryCardType (
  `name`       varchar(16) NOT NULL,
  `maName`     varchar(16) NOT NULL REFERENCES manufacturer,
  `typeName`   varchar(60) NOT NULL REFERENCES equipmentType,
  PRIMARY KEY(name)
);


#==============================================================================#
#                           INSERTING_VALUES                                   #
#==============================================================================#

#========STORE=========#
INSERT INTO store VALUES
(1, 'FotoRental Bergen', 'bergen@fotorental.no', 'Storgata 10, 5835 Bergen');
INSERT INTO store VALUES
(2, 'FotoRental Kristiansand', 'kristiansand@fotorental.no', 'Storgata 10, 4604 Kristiansand');


#========CUSTOMER=========#
INSERT INTO customer VALUES
('ola@nordmann.com', 'Ola Nordmann', '+4799999999', 'Langrand 51, 3055 Krokstadelva');
INSERT INTO customer VALUES
('kari@nordmann.com', 'Kari Nordmann', '+4744444444', 'Vestpå 1, 5003 Bergen');
INSERT INTO customer VALUES
('per@nordmann.com', 'Per Nordmann', '+4791919191', 'Sørpå 1, 4513 Mandal');
INSERT INTO customer VALUES
('fjstian@gmail.com', 'Stian Fjerdingstad', '+4794196705', 'Dovregubben 36, 3055 Krokstadelva');


#========TELEPHONE=========#
INSERT INTO telephone VALUES
(1, '+4755555555');
INSERT INTO telephone VALUES
(1, '+4743211234');
INSERT INTO telephone VALUES
(2, '+4738383838');


#========ASSESSER=========#
INSERT INTO assesser VALUES
(1, 'Kameratakst', 'post@kameratakst.no', '+4799119911', 'Strandveien 22, 4630 Kristiansand');
INSERT INTO assesser VALUES
(2, 'C&V Teknikk AS', 'service@camera.no', '+4755393880', 'Liaveien 1, 5132 Nyborg');


#========MANUFACTURER=========#
INSERT INTO manufacturer VALUES
('Nikon', 'JP', 'https://nikoneurope-no.custhelp.com/app/answers/list/locale/no_NO');
INSERT INTO manufacturer VALUES
('Sony', 'JP', 'https://www.sony.no/support/no/hub/prd-dime-alpha');


#========ASSESES=========#
INSERT INTO asseses VALUES
('Nikon', 1, 1, '2014-01-01');
INSERT INTO asseses VALUES
('Nikon', 1, 2, '2014-01-01');
INSERT INTO asseses VALUES
('Sony', 1, 1, '2014-01-01');
INSERT INTO asseses VALUES
('Sony', 1, 2, '2014-01-01');
INSERT INTO asseses VALUES
('Sony', 2, 1, '2016-01-01');


#=======LENS TYPE=========#
INSERT INTO lensType VALUES
(1, '58', 1.4, 70, 72);
INSERT INTO lensType VALUES
(2, '50', 1.4, 43, 55);
INSERT INTO lensType VALUES
(3, '50', 1.4, 71, 72);


#=======MEMORY CARD TYPE========#
INSERT INTO memoryCardType VALUES
('Compact Flash', 'Nikon', 'D4');
INSERT INTO memoryCardType VALUES
('XQD', 'Nikon', 'D4');


#========LEASE=========#
INSERT INTO lease VALUES
(1 , 1, '2013-01-10', '2013-01-06', NULL, 'finished', 'ola@nordmann.com');
INSERT INTO lease VALUES
(2 , 1, '2014-11-10', '2014-11-12', 750, 'finished', 'kari@nordmann.com');
INSERT INTO lease VALUES
(2 , 2, '2016-10-10', '2016-10-30', NULL, 'active', 'per@nordmann.com');


#========PRICE=========#
INSERT INTO price VALUES
(2, 1, 250, "UV-filter ødelagt");


#=======LENS MOUNT=========#
INSERT INTO lensMount VALUES
(1, 'Nikon F-mount', 'Nikon');
INSERT INTO lensMount VALUES
(2, 'Sony A-mount', 'Sony');


#=======EQUIPMENT TYPE=========#
INSERT INTO equipmentType VALUES
('Nikon', 'D4', 1180, 300.00, 16.4, NULL, 1);
INSERT INTO equipmentType VALUES
('Nikon', 'AF-S Nikkor 59mm f/1.4G', 385, 150.00, NULL, 1, 1);
INSERT INTO equipmentType VALUES
('Sony', 'Alfa 900', 850, 200.00, 24.6, NULL, 2);
INSERT INTO equipmentType VALUES
('Sony', '50mm f/1.4', 220, 100.00, NULL, 2, 2);
INSERT INTO equipmentType VALUES
('Sony', 'Planar T* FE 50mm F1.4 ZA', 778, 150.00, NULL, 3, 2);


#========EQUIPMENT=========#
INSERT INTO equipment VALUES
(11112222, '2012-12-12', 'working', 3, 'Nikon', 'D4', 1, 1);
INSERT INTO equipment VALUES
(11113333, '2015-08-10', 'working', 1, 'Nikon', 'AF-S Nikkor 59mm f/1.4G', 1, NULL);
INSERT INTO equipment VALUES
(22224444, '2012-12-12', 'working', 2, 'Sony', 'Alf1 900', 2, 2);
INSERT INTO equipment VALUES
(22225555, '2013-11-11', 'defect', 3, 'Sony', '50mm f/1.4', 2, NULL);
INSERT INTO equipment VALUES
(22227777, '2014-09-22', 'working', 2, 'Sony', '50mm f/1.4', 2, 1);
INSERT INTO equipment VALUES
(22229999, '2015-11-30', 'working', 1, 'Sony', 'Planar T* FE 50mm F1.4 ZA', 2, NULL);
